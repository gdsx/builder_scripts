#!/usr/bin/env bash

# shellcheck disable=SC2012

declare -A collected_deps

collect_depts() {
    local dir=$1
    local bin=$2
    local out=$3

    if [[ ${collected_deps[$bin]} == 1 ]]; then
       # echo "~ : $bin already checked. Skip"
       return 1
    fi
    collected_deps[$bin]=1


    local deps=
    local pth="$dir/$bin"

    if [[ -f $pth ]]; then
        # echo "! $bin"
         echo "$bin" >> "$out"
        deps=$(objdump -x "$pth"|grep Name:|awk -F": " '{print $2}')
        IFS=$'\n' deps=($deps)
        for dep in "${deps[@]}"; do
            if [[ ${collected_deps[$dep]} == 1 ]]; then
                continue
            fi
            collect_depts "$dir" "$dep" "$out"
        done
    # else
    #    echo ">> : $bin LEAF"
    fi
}

check_dep() {
    bin_dir=$(realpath --no-symlinks "$1")
    preserves="$2"
    # # DIR_NAME="${BASH_SOURCE%[/\\]*}"

    # # echo "$bin_path"
    # # echo "$preserves"

    # shopt -s globstar  # enable recursive globs
    # # bin_path="d:\dev\repo\builder\_scripts\_working\wood\work_debug\distr\bin"
	# # exe="$bin_path/*.exe"
	# # dll="$bin_path/*.dll"
    # # dep_txt="$WORK/dependencies.txt"

	# # dumpbin //DEPENDENTS "$exe" "$dll" 1>"$WORK/dep_orig.txt"




    # # TTL 1 php "$DIR_NAME"/php/check_dep.php "$bin_dir" "$preserves" "$WORK"



    # Works but slow
    #
    IFS=';' preserves=($preserves)

    deps_txt="$WORK/dep_preserve.txt"
    echo -n "" > "$deps_txt"
    for preserve in "${preserves[@]}"; do
       echo "## Collecting dependecies: $preserve"
       collect_depts "$bin_dir" "$preserve" "$deps_txt"
    done

    sort -u "$deps_txt" -o "$WORK/dep_preserve_uniq.txt"


    # pushd "$bin_dir" 1> /dev/null || exit
    # all=$(ls -- *.dll *.exe)
    # IFS=$'\n' all=($all)
    # for bin in "${all[@]}"; do
    #     echo "## Collecting dependecies: '$bin'"

    #     deps=$(dumpbin //DEPENDENTS "$bin")
    #     echo "${deps[@]}" > "$WORK/1.txt"

    #     deps=$(objdump -x "$bin"|grep Name:|awk -F": " '{print $2}')
    #     echo "${deps[@]}" > "$WORK/2.txt"

    # done
    # popd 1> /dev/null || exit
    # # exit




    pushd "$bin_dir" 1> /dev/null || exit
    ls -- *.dll *.exe | sort -o "$WORK/dep_exist.txt"
    popd 1> /dev/null || exit


    echo "## Drop unused binaries"
    while read -r name; do
        echo "! Drop: $name"
        # unlink "$bin_dir/$name"
    done < <(comm -13 "$WORK/dep_preserve_uniq.txt" "$WORK/dep_exist.txt")


    echo "## Find system dependencies"
    while read -r name; do
        out=$(which "$name" 2>&1)
        echo "! Find: $name"
        if [[ $out =~ (^which:\ ) ]]; then
            echo "WARNING: '$name' is MISSING"
        fi
    done < <(comm -23 "$WORK/dep_preserve_uniq.txt" "$WORK/dep_exist.txt")





    # echo "## Keep dependencies"
    # while read -r name; do
    #     # echo "~ Keep: $name"
    # done < <(comm -12 "$WORK/dep_preserve_uniq.txt" "$WORK/dep_exist.txt")



    # diff "$WORK/dep_preserve_uniq.txt" "$WORK/dep_exist.txt"


    # IFS=';' exist=("$exist")

    # for bin in "${exist[@]}"; do
    #     if [[ -f $bin_dir/$bin ]]; then
    #         echo "! $bin OK"
    #     else
    #         echo "ERROR: $bin Missing"
    #     fi
    # done
    # echo ""


    # IFS=$'\n' dep_lists=($OUTPUT)
    # localbins=${dep_lists[1]}
    # sysbins=${dep_lists[3]}
    # deletes=${dep_lists[5]}

    # exist=$(find "$bin_path" -iname '*.exe' -or -iname '*.dll' -printf '%f;')

    # IFS=';' exist=($exist)
    # IFS=';' localbins=($localbins)
    # IFS=';' sysbins=($sysbins)

    # # echo -e "~ EXIST\n${exist[@]}\n"
    # # echo -e "~ LOCAL\n${localbins[@]}\n"
    # # echo -e "~ SYSTEM\n${sysbins[@]}\n"

    # # for ($bin in ${exist[@]}); do
    # for bin in ${exist[@]}; do
    #     if [[ -f $bin ]]; then
    #         echo -n "$bin "
    #     fi
    # done
    # echo ""

    # //echo "check dir: [$distr_bin_path*]\n";
    # $removes     = dependList($command_log, $worker_id, "$bin_path", "$preserves", 1);
    # $remove_list = explode(";", $removes);

    # // Remove unused binaries

    # foreach ($remove_list as $file_name) if (strlen($file_name)>1)
    # {
    # 	$file_path = "$distr_bin_path/$file_name";
    # 	unlink("$file_path");
    # 	_log_to($command_log, "Removing dependency: [$file_path]");
    # }

    # // Cheking preserve list";
    # checkDependList($command_log, $distr_bin_path, $preserves);

    # // Cheking dll's in pakage
    # // There are missing:";

    # $missing = dependList($command_log, $worker_id, "$bin_path", "$preserves", 3); // type3 shows missing dll's
    # if (strlen($missing) > 4)
    # 	_log_to($command_log, "WARNING: Some files missing [$missing]");
}
