#!/usr/bin/env bash

# build_number - Calculate build number by days elapsed from birth
# $1: birth   - The ISO date when project was started

build_number() {
    local birth today build
    birth=$(date --date="$1" +%s)
    today=$(date -u +%s)
    days=$((($today-$birth)/86400))
    echo "$days"
}

