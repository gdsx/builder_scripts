#!/usr/bin/env bash

# update_version - update version in resources of binary file
# $1: bin_dir - binaries *.exe and *.dll folder
# $2: version - Format 1.2
# $3: company - Company, can be empty

update_version() {
    local bin_dir version company DIR_NAME
    bin_dir=$(realpath --no-symlinks "$1")
    version="$2"
    company="$3"
    DIR_NAME="${BASH_SOURCE%[/\\]*}"
    php "$DIR_NAME"/update_version.php "$bin_dir" "$version" "$company"
}
