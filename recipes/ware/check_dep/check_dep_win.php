<?php

// $system_dll = 'KERNEL32.dll MSVCRT.dll comdlg32.dll ole32.dll COMCTL32.dll SHELL32.dll '.
// 			'OLEAUT32.dll ADVAPI32.dll USER32.dll GDI32.dll MFC42.DLL MSVCP60.dll '.
// 			'WINSPOOL.DRV WS2_32.dll oledlg.dll ODBC32.dll WINMM.dll RPCRT4.dll '.
// 			'CRPE32.dll SHLWAPI.dll MSIMG32.dll WSOCK32.dll GDIPLUS.dll ATL.DLL '.
// 			'MSCOREE.DLL UXTHEME.DLL';

function load_dependencies($path) {
	$bin2dep = Array();
	$root = "";
	$lines = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
	foreach ($lines as $line) {
		$line = trim($line);
        if (stripos($line, "Dump of file ") === 0) {
			$fname = basename($line);
			$root = $fname;
			$bin2dep[$root] = Array();
			continue;
		}
		if (!$root ||
			$line === "Image has the following dependencies:" ||
			$line === "Image has the following delay load dependencies:" ||
			$line === "\f" ||
		    stripos($line, "File Type: ") === 0 ||
		    stripos($line, "LINK : ") === 0
		) {
			continue;
		}
        if ($line === "Summary") {
			break;
		}
		$bin2dep[$root][] = $line;
    }
	return $bin2dep;
}

function analyze_branch($bin2deps, $bin, &$parents, &$leafs) {
	// $bin = strtolower($bin);
	if (isset($bin2deps[$bin])) {
		$parents[$bin] = 1;
		foreach ($bin2deps[$bin] as $dep) {
			analyze_branch($bin2deps, $dep, $parents, $leafs);
		}
	}
	else {
		$leafs[$bin] = 1;
	}
}

function analyze_dependencies($dep_txt, $preserves, &$parents, &$leafs)
{
	$bin2deps  = load_dependencies($dep_txt);
	// print_r($bin2deps);
	$preserves = explode(';', $preserves);
	$parents   = Array();
	$leafs     = Array();

	foreach ($preserves as $bin) {
		analyze_branch($bin2deps, $bin, $parents, $leafs);
	}

	$parents = array_keys($parents);
	$leafs   = array_keys($leafs);
	sort($parents);
	sort($leafs);
}


$bin_dir   = $argv[1];
$preserves = $argv[2];
$work_dir  = $argv[3];
$dep_txt   = "$work_dir/dep_dump.txt";

echo "~ Bin dir:      $bin_dir\n";
echo "~ Dependencies: $dep_txt\n";
echo "~ Preserves:    $preserves\n";

if (is_file($dep_txt)) {
	unlink($dep_txt);
}

$exe = "$bin_dir/*.exe";
$dll = "$bin_dir/*.dll";
shell_exec("dumpbin /DEPENDENTS \"$exe\" \"$dll\" > \"$dep_txt\"");

analyze_dependencies($dep_txt, $preserves, $parents, $leafs);

// echo "! PARENTS:\n";
// foreach ($parents as $parent) echo "$parent;";
// echo "\n! LEAFS:\n";
// foreach ($leafs as $leaf) echo "$leaf;";
// echo "\n";

$exists = array_map('basename', glob("$bin_dir/*.{exe,dll}", GLOB_BRACE));

$deletes = array_udiff($exists,  $parents, 'strcasecmp');
$deletes = array_udiff($deletes, $leafs,   'strcasecmp');
// print("! DELETES: ");
// print_r($deletes);

foreach ($deletes as $delete) {
	$res = @unlink("$bin_dir/$delete");
	if ($res) {
		echo "! The file '$delete' was deleted!\n";
	} else {
		echo "WARNING: The file '$delete' was not deleted!\n";
	}
}

foreach ($leafs as $leaf) {
	if (is_file("$bin_dir/$leaf")) {
		continue;
	}
    $which = shell_exec("which $leaf 2>&1");
	if (stripos("$which", "which:") === 0) {
		echo "ERROR: Missing dependency '$leaf'\n";
	}
}

// foreach ($exists as $exist) {
// 	echo "$exist ";
// }
// echo "\n";

// if ($handle = opendir($bin_dir)) {
// 	while (false !== ($entry = readdir($handle))) {
// 		if ($entry == "." || $entry == "..") {
// 			continue;
// 		}
// 		echo "$entry\n";
// 	}

// 	closedir($handle);
// }

?>
