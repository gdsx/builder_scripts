#!/usr/bin/env bash

check_dep() {
    bin_dir=$(realpath --no-symlinks "$1")
    preserves="$2"
    DIR_NAME="${BASH_SOURCE%[/\\]*}"

    php "$DIR_NAME"/check_dep_win.php "$bin_dir" "$preserves" "$WORK"
}
