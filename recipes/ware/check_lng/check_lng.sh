#!/usr/bin/env bash

# check_lng - if language files used by exes and dlls, remove not used language files
# $1: lng_path - language files *.lng folder
# $2: bin_path - binaries *.exe and *.dll folder

check_lng() {
    lng_dir=$(realpath --no-symlinks "$1")
    bin_dir=$(realpath --no-symlinks "$2")
    DIR_NAME="${BASH_SOURCE%[/\\]*}"

    php "$DIR_NAME"/check_lng.php "$lng_dir" "$bin_dir" "$WORK"
}
