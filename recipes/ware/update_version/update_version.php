<?php
// Update version for exe and dll binaries
// -------------
// $1 bin_path - binaries *.exe and *.dll folder
// $2 version  - Format "num.num.num.num"
// $3 company  - Version will be updated only for files from this company
// -------------

$bin_path   = $argv[1];
$version    = $argv[2];
$company    = $argv[3];
$exe_filter = "$bin_path/*.exe";
$dll_filter = "$bin_path/*.dll";

echo "-----------------------------------------\n";
echo "> COMPANY       = [$company]\n";
echo "> VERSION       = [$version]\n";
echo "> EXE filter    = [$exe_filter]\n";
echo "> DLL filter    = [$dll_filter]\n";
echo "-----------------------------------------\n";

$exes       = glob("$exe_filter");       //full paths of existing exes
$dlls       = glob("$dll_filter");       //full paths of existing dlls
$bins       = array_merge($exes, $dlls); //full paths of existing exes & dlls
$bins_count = count($bins);

if (!$bins_count) {
	echo "ERROR: No binaries found\m";
	return;
}

foreach($bins as $index => $bin) {
	$nr          = $index + 1;
	$bin_name    = basename($bin);
    $company_out = ''; //required because on fail it is not changed
    $cmd_get     = "verpatch \"$bin\" /vo | grep CompanyName | awk -F'\"' '{print $4}'";

    exec($cmd_get, $curr_company);

    $curr_company = isset($curr_company[0]) ? $curr_company[0] : false;

    //FIXME: remove in production after resources fix
    if ($curr_company === "\\0") {
        $curr_company = '';
    }

    // echo "[$nr] $bin_name ==> company '$curr_company' ~=~ '$company'\n";

    if (!empty($curr_company) && $curr_company !== $company) {
        echo "~ [$nr] $bin_name ==> company '$curr_company' SKIPPED \n";
        continue;
    }

    $cmd_set = '';
    if ($curr_company === false) {
        $cmd_set = "verpatch \"$bin\" /va $version";
        echo "! [$nr] $bin_name ==> {$version} => CREATE resource\n";
    }
    else {
        $cmd_set = "verpatch \"$bin\" $version";
        echo "[$nr] $bin_name ==> {$version} => update\n";
    }

    $set_out = '';
    exec($cmd_set, $set_out, $set_ret);

    if ($set_ret !== 0) {
        echo "ERROR: Writing version to '$bin_name' failed [$cmd_set] [output: $set_out[0]]\n";
    }
}

echo "-----------------------------------------\n";
echo "[DONE]\n";
?>
