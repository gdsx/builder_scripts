<?php

$lng_dir  = $argv[1];
$bin_dir  = $argv[2];
$work_dir = $argv[3];
$lng_list = "$work_dir/lng_dump.txt";

echo "~ Lng dir:  $lng_dir\n";
echo "~ Bin dir:  $bin_dir\n";
echo "~ lng_list: $lng_list\n";

if (is_file($lng_list)) {
	unlink($lng_list);
}

$exe_filter = "$bin_dir/*.exe";
$dll_filter = "$bin_dir/*.dll";
$lng_filter = "$lng_dir/*/*.lng";

echo "! -----------------------------------------\n";
echo "! Language path: [$lng_dir]\n";
echo "! Binaries path: [$bin_dir]\n";
echo "! -----------------------------------------\n";
echo "! EXE filter: [$exe_filter]\n";
echo "! DLL filter: [$dll_filter]\n";
echo "! LNG filter: [$lng_filter]\n";
echo "! LNG list:   [$lng_list]\n";
echo "! -----------------------------------------\n";

$exes       = glob("$exe_filter");       //full paths of existing exes
$dlls       = glob("$dll_filter");       //full paths of existing dlls
$bins       = array_merge($exes, $dlls); //full paths of existing exes & dlls
$lngs       = glob("$lng_filter");       //full paths of all existing lngs
$bins_count = count($bins);
$lngs_count = count($lngs);
$lngs_used  = array();                   //names of lngs used by dlls

if (!$bins_count)
{
	// echo "ERROR: No binaries found");
	return;
}


// echo "-----------------------------------------", false);
echo "Collecting names of languages used by binaries...\n";
// echo "");

foreach($bins as $index => $bin)
{
	$nr       = $index + 1;
	$bin_name = basename("$bin");

    // echo "* $bin_name\n";

// 	$exe = realpath("../cmd/ListLng.exe");
// 	$run = "_cmd_lng";
// 	$cmd = "$run /c $exe $bin >\"$lng_list\" 2>>\"$command_log\"";

// 	if (!run_process($cmd, $result, NULL, 1*60))
// 		return;

// 	if (0 == $result)
// 	{
// 		$lines = file("$lng_list", FILE_IGNORE_NEW_LINES);

// 		if ($lines && isset($lines[0]) && !empty($lines[0]))
// 		{
// 			$lng = $lines[0];
// 			echo "[$nr/$bins_count] $bin_name --> $lng");
// 			$lngs_used[] = $lng;
// 		}
// 		else
// 			echo "[$nr/$bins_count] $bin_name --> None");
// 	}
// 	else
// 		echo "ERROR: [$nr/$bins_count] [$bin_name] FAILED to detect lng file");
// }

// echo "-----------------------------------------", false);
// echo "Grouping existing languages by folders and collecting unique names...");
// echo "");

// $lngs_exist = [];
// $lngs_exist_by_dir = [];
// foreach($lngs as $lng)
// {
// 	$lng_name                      = basename($lng);
// 	$lng_dir                       = dirname($lng);
// 	$lngs_exist_by_dir[$lng_dir][] = $lng_name;
// 	$lngs_exist[]                  = $lng_name;
}

// sort($lngs_exist);
// $lngs_exist = array_unique($lngs_exist);
// //echo implode(', ', $lngs_exist));


// echo "-----------------------------------------", false);
// echo "Checking for missmaching languages between different language folders (LT/NL/etc.)...");
// echo "");

// foreach($lngs_exist_by_dir as $lng_dir => $lng_names)
// 	if ($missing_in_dir = array_udiff($lngs_exist, $lng_names, 'strcasecmp'))
// 		echo "NOTICE: Language missmach, missing in [$lng_dir]: " . implode(', ', $missing_in_dir));


// echo "-----------------------------------------", false);
// echo "Checking for not used languages...");

// foreach($lngs_exist_by_dir as $lng_dir => $lng_names)
// 	if ($removes = array_udiff($lng_names, $lngs_used, 'strcasecmp'))
// 	{
// 		echo "");

// 		foreach($removes as $remove)
// 		{
// 			echo "Removing: $lng_dir\\$remove");

// 			if (!unlink("$lng_dir\\$remove"))
// 				echo "ERROR: Failed to remove not used language [$lng_dir\\$remove]");
// 		}
// 	}

// echo "-----------------------------------------", false);
// echo "Checking for missing languages...");

// foreach($lngs_exist_by_dir as $lng_dir => $lng_names)
// 	if ($missings = array_udiff($lngs_used, $lng_names, 'strcasecmp'))
// 	{
// 		echo "");

// 		foreach($missings as $missing)
// 			echo "ERROR: MISSING language [$lng_dir\\$missing]");
// 	}


// echo "-----------------------------------------", false);
// echo "[DONE]");

?>
